import unittest
import factorial as f

class TestFactorial(unittest.TestCase):
	def test_fact_return_correct(self):
		result = f.fact(5)
		self.assertEqual(result, 120)

	def test_div_return_correct(self):
		result = f.div(2, 2)
		self. assertEqual(result, 1)

	def test_div_zero(self):
		self.assertRaises(ZeroDivisionError, f.div, 1, 0)

if __name__ == "__main__":
	unittest.main()
