import unittest
from calculate import Calculate

class TestCalculate(unittest.TestCase):
	def setUp(self):
		self.calc = Calculate()

		def test_add_ret_correctly(self):
			self.assertEqual(4, self.calc.add(2,2))

		def test_add_strings_raise_error(self):
			self.assertRaises(TypeError, self.calc.add, "a", "b")

if __name__ == "__main__":
	unittest.main()
