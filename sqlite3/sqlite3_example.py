
import sqlite3

db1 = sqlite3.connect('itsugar.db')

cursor = db1.cursor()

cursor.execute('CREATE TABLE employee (emp_id INTEGER, fname TEXT, lname TEXT, pay_rate REAL)')

db1.commit()

record = (1, 'Bob', 'Rickard', 2.00)

cursor.execute('INSERT INTO employee VALUES (?,?,?,?)', record)

db1.commit()

for row in cursor.execute('SELECT * FROM employee'):
    print(row)

new_employees = [ (2, 'Mike', 'Jones', 0.72), (3, 'Nicki', 'Minaj', 1.75), (4, 'Cave', 'Johnson', 900.25) ]

cursor.executemany('INSERT INTO employee VALUES (?,?,?,?)', new_employees)

db1.commit()

for row in cursor.execute('SELECT * FROM employee where pay_rate > .75'):
    print(row)

target_pay = 2.5
for row in cursor.execute('SELECT * FROM employee where pay_rate > ?', (target_pay,)):
     print(row)

cursor.execute('UPDATE employee SET pay_rate=10 WHERE emp_id=1')

db1.commit()


for row in cursor.execute('SELECT * FROM employee ORDER BY pay_rate DESC'):
    print(row)

for row in cursor.execute('SELECT * FROM employee WHERE pay_rate > 5 AND pay_rate < 100'):
    print(row)

cursor.execute('CREATE TABLE location (loc_id INTEGER, name TEXT)')

db1.commit()

cursor.execute('ALTER TABLE employee ADD COLUMN loc_id INTEGER')

db1.commit()

for row in cursor.execute('SELECT * FROM employee ORDER BY pay_rate DESC'):
    print(row)

locs = [ (1, 'Cancun'), (2, 'Miami Beach'), (3, 'Endor') ]

cursor.executemany('INSERT INTO location VALUES (?,?)', locs)

db1.commit()

for num in range(5):
    theNum = num
    idNum = theNum
    if theNum > 3:
        theNum %= 3
    cursor.execute('UPDATE employee SET loc_id = ? WHERE emp_id = ?', (theNum, idNum))

cursor.execute('SELECT employee.fname, location.name FROM employee, location WHERE employee.loc_id = location.loc_id')

for row in cursor.execute('SELECT employee.fname, location.name FROM employee, location WHERE employee.loc_id = location.loc_id'):
    print(row)

row = cursor.execute('SELECT DISTINCT loc_id FROM employee')

print(tuple(row))

# cursor.execute('ALTER TABLE employee ADD(CONSTRAINT pk_emp_id PRIMARY KEY(emp_id) )')
# cursor.execute('ALTER TABLE employee ADD(CONSTRAINT fk_loc_id FOREIGN KEY(loc_id) REFERENCES location(loc_id))')
# cursor.execute('ALTER TABLE location ADD(CONSTRAINT pk_loc_id PRIMARY KEY(loc_id) )')



