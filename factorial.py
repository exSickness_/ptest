import sys

def fact(n):
	if( n == 0 ):
		return(1)
	return( n * fact(n-1) )

def div(x, y):
	return( x / y )
